import json
import requests
from requests.api import head
from requests.models import HTTPBasicAuth


ticketId = 1296

url = "https://aainsurance1619645175.zendesk.com/api/v2/tickets/" + str(ticketId)

payload = {}

headers = {
    'Authorization' : 'Basic ZXVuaWNlX3NhYmRhb0BhYWluc3VyYW5jZS5jby5uejozMm83Y2VoVmJMaGkh'
}

response = requests.request("GET", url, headers = headers, data = payload)

prettyJson = json.loads(response.text)

ticketCustomFields = prettyJson["ticket"]["custom_fields"]
#print(ticketCustomFields)


ariaQueryIndex = next((i for i, item in enumerate(ticketCustomFields) if item["id"] == 360011562395), None)
print(ariaQueryIndex)

utterance = ticketCustomFields[ariaQueryIndex]["value"]
print("Utterance: " + utterance)
